console.log("Consola de pruebas de log in")

import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, ref, set, push, onValue} from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js';


const firebaseConfig = {
    apiKey: "AIzaSyDizIBd79erlLEQOweh8UWj81F3IbYt6zs",
    authDomain: "funn-c381a.firebaseapp.com",
    databaseURL: "https://funn-c381a-default-rtdb.firebaseio.com",
    projectId: "funn-c381a",
    storageBucket: "funn-c381a.appspot.com",
    messagingSenderId: "833880545945",
    appId: "1:833880545945:web:2a12803d83da8b0bdfb608"
  };

const app = initializeApp(firebaseConfig);
const auth = getAuth(); 
const database = getDatabase();
//registro

let nameRef = document.getElementById("nameId");
let surnameRef = document.getElementById("surnameId");
let cityRef = document.getElementById("cityId");
let correoRef = document.getElementById("direccionCorreoId");
let passRef = document.getElementById("passwordId");
let buttonRef = document.getElementById("registroButtonId");
 
//logueo

let correoLoginRef = document.getElementById("loginCorreoId");
let passLoginRef = document.getElementById("loginPasswordId");
let loginRef = document.getElementById("ingresarButtonId");

// Event Listeners de registro y logueo

buttonRef.addEventListener("click", registroUsuario);
//buttonRef.addEventListener("click", cargarInformacion);
loginRef.addEventListener("click", logIn);


// Promesas: https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Using_promises
//           https://developer.mozilla.org/es/docs/Glossary/Callback_function
//           https://www.youtube.com/watch?v=slIJj-zbs_M

function registroUsuario(){

    console.log("Ingreso a la función registroUsuario().");

    if((correoRef.value != '') && (passRef.value != '')){

        // API Docs. https://firebase.google.com/docs/reference/js/auth.md?authuser=0&hl=en#createuserwithemailandpassword
        // let variable = createUserWithEmailAndPassword(auth, correoRef.value, passRef.value);
        // console.log(variable);
        // https://www.w3schools.com/js/js_arrow_function.asp

        createUserWithEmailAndPassword(auth, correoRef.value, passRef.value)

        .then((userCredential) => {
            // Signed in
            const user = userCredential.user;
     
            console.log("Usuario: " + user + " ID: " + user.uid);
            console.log("Creación de usuario.");

            var name = nameRef.value;
            var surname = surnameRef.value;
            var city = cityRef.value;

            set(ref(database, 'Usuarios/' + user.uid), {
                nombre: name, 
                apellido: surname, 
                ciudad: city
            }).then(() => {
                window.location.href = "../Home.html";
            })

            //correoRef.value = '';
            //passRef.value = '';
            //window.location.href = "../Home.html";

        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
        });
    }
    else{
        alert("Revisar que los campos de usuario y contraseña esten completos.");
    }    
}


function logIn (){

    console.log("Ingreso a la función logIn().");

    if((correoLoginRef.value != '') && (passLoginRef.value != '')){

        signInWithEmailAndPassword(auth, correoLoginRef.value, passLoginRef.value)
        .then((userCredential) => {
            // Signed in
            const user = userCredential.user;
            window.location.href = "../Home.html";
            // ...
        })
        .catch((error) => {
            const errorCode = error.code;
            const errorMessage = error.message;
            console.log("Código de error: " + errorCode + " Mensaje: " + errorMessage);
        });
    }
    else{
        alert("Revisar que los campos de usuario y contraseña esten completos.");
    }    

}


