// Módulos js: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Statements/import
//             https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Modules#aplicar_el_m%C3%B3dulo_a_tu_html

console.log("Consola de pruebas de home ")

import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, onAuthStateChanged, signOut} from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, ref, set, push, onValue, query, orderByKey, get} from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js';


const firebaseConfig = {
    apiKey: "AIzaSyDizIBd79erlLEQOweh8UWj81F3IbYt6zs",
    authDomain: "funn-c381a.firebaseapp.com",
    databaseURL: "https://funn-c381a-default-rtdb.firebaseio.com",
    projectId: "funn-c381a",
    storageBucket: "funn-c381a.appspot.com",
    messagingSenderId: "833880545945",
    appId: "1:833880545945:web:2a12803d83da8b0bdfb608"
};

const app = initializeApp(firebaseConfig);
const database = getDatabase(app);
const auth = getAuth(); //09.07.22

const user = auth.currentUser;

var correo;

//referencias al html
let chatRef = document.getElementById("mensajesChatId");
let textoRef = document.getElementById("textoChatId");
let enviarRef = document.getElementById("buttonChatId");

//escuchadores de eventos
//agregado 09-07

enviarRef.addEventListener("click", enviarMensaje);

let logOut = document.getElementById("logOutButton");
logOut.addEventListener("click", loggingOut);

var usuario;

onAuthStateChanged(auth, (user) => {
    if (user) {
      // User is signed in.
      correo = user.email;
      console.log(user.uid);
      get(ref(database, "Usuarios/" + user.uid)).then((user) => {
        usuario = user.val();
        console.log(usuario);
      })
    }
    else {
      // User is signed out.
      console.log("no esta el usuario");
    }
  })
  
function enviarMensaje(){
    console.log("Ingreso a la función enviarMensaje");
    let mensaje = textoRef.value;

    push(ref(database, "mensajes/"), {
        msg: mensaje,
        sender: usuario.nombre + " " + usuario.apellido,
        city: usuario.ciudad
    })

    textoRef.value= "";
    console.log("Mensaje enviado");
}

const queryMensajes = query(ref(database, 'mensajes/'), orderByKey('msg'));
console.log(queryMensajes);

onValue(queryMensajes, (snapshot)=> {

    chatRef.innerHTML = '';

    snapshot.forEach((childSnapshot) => {
        let message = childSnapshot.val();
    chatRef.innerHTML += `
        <p>${message.sender} (${message.city}): ${message.msg}</p>
    `;
    
    });
});

function loggingOut(){
    console.log("Acceso a loggingOut");
    auth.signOut();
    location.reload(true);

//    const auth = getAuth();
//    singOut(auth).then(()=>{
//    window.location.href="../Home.html";
//    })
//    .catch((error) => {
//    });

}